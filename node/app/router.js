'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
    const { router, controller, middleware } = app;

    router.post('/api/upload/file', controller.uploadcos.cos);
    // 任务
    router.get('/api/task/list', controller.task.allList);
    router.resources('task', '/api/task', controller.task);
    // 接单记录
    router.get('/api/taskRecord/acceptList', controller.taskRecord.acceptList); // 我的订单
    router.get('/api/taskRecord/status', controller.taskRecord.status); // 我的任务状态
    router.post('/api/taskRecord/accept', controller.taskRecord.accept); // 接单
    router.post('/api/taskRecord/finish', controller.taskRecord.finish); // 完成
    router.post('/api/taskRecord/cancel', controller.taskRecord.cancel); // 取消
    router.get('/api/taskRecord/auditList', controller.taskRecord.auditList); // 审核列表
    router.post('/api/taskRecord/audit', controller.taskRecord.audit); // 审核
    router.resources('taskRecord', '/api/taskRecord', controller.taskRecord);
    // 用户
    router.get('/api/user/list', controller.user.allList);
    router.post('/api/user/audit', controller.user.audit); // 指定用户为审核人员
    router.post('/api/user/login', controller.user.login); // 登录
    router.resources('user', '/api/user', controller.user);
    // balance
    router.get('/api/balance/list', controller.balance.allList);
    router.post('/api/balance/create2', controller.balance.create2); // 提现
    router.resources('balance', '/api/balance', controller.balance);
};

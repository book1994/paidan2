'use strict';

module.exports = {
  shop_id: {
    type: 'number',
    required: true,
  },
  role_ids: {
    type: 'array',
    itemType: 'number',
    required: true,
  },
};

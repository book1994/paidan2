const dayjs = require("dayjs");
module.exports = app => {
    const { STRING, INTEGER, TEXT,DECIMAL } = app.Sequelize;
    const TaskRecord = app.model.define(
        'taskRecord',
        {
            id: {type: INTEGER, primaryKey: true, autoIncrement: true},
            task_record_no: STRING(255),
            task_id: INTEGER,
            user_id: INTEGER, // 接单人
            step_list: TEXT, // 步骤记录
            audit_user_id: INTEGER, // 审核人
            status: INTEGER, // 1 已接单、0放弃
            audit_status: INTEGER, // -1 待审核 0 拒绝 1 通过
            price: DECIMAL(10,2), // 价格
            end_time: {
                type: app.Sequelize.DATE,
                allowNull: true,
                get() {
                    return dayjs(this.getDataValue('end_time')).format('YYYY-MM-DD HH:mm:ss');
                },
            },
            create_time: {
                type: app.Sequelize.DATE,
                allowNull: false,
                get() {
                    return dayjs(this.getDataValue('create_time')).format('YYYY-MM-DD');
                },
            },
            updated_time: {
                type: app.Sequelize.DATE,
                allowNull: false,
                get() {
                    return dayjs(this.getDataValue('updated_time')).format('YYYY-MM-DD');
                },
            }
        },
        {
            tableName: 'task_record',
            timestamps: true, // 启用时间戳(createdAt, updatedAt)
            updatedAt: 'updated_time', // 不想要 updatedAt
            createdAt: 'create_time', // 想要  createdAt 但是希望名称叫做 created_at
        }
    );
    TaskRecord.associate = function() {
        app.model.TaskRecord.belongsTo(app.model.Task, {foreignKey: 'task_id', targetKey: 'id', as: 'task'});
        app.model.TaskRecord.belongsTo(app.model.User, {foreignKey: 'audit_user_id', targetKey: 'id', as: 'auditUser'});
        app.model.TaskRecord.belongsTo(app.model.User, {foreignKey: 'user_id', targetKey: 'id', as: 'user'});
    }
    return TaskRecord
};

// 生成mysql建表语句
// CREATE TABLE `task_record` (
//     `id` int(11) NOT NULL AUTO_INCREMENT,
//     `task_group_id` int(11) DEFAULT NULL COMMENT '任务组ID',
//     `task_id` int(11) DEFAULT NULL COMMENT '任务ID',
//     `factory_id` int(11) DEFAULT NULL COMMENT '工厂ID',
//     `supplier_id` int(11) DEFAULT NULL COMMENT '供应商ID',
//     `supplier_user_id` int(11) DEFAULT NULL COMMENT '供应商用户ID',
//     `supplier_name` varchar(255) DEFAULT NULL COMMENT '供应商名称',
//     `supplier_user_name` varchar(255) DEFAULT NULL COMMENT '供应商用户名称',
//     `supplier_user_phone` varchar(255) DEFAULT NULL COMMENT '供应商用户电话',
//     `remark` text COMMENT '备注',
//     `files` text COMMENT '文件',
//     `finish_time` datetime DEFAULT NULL COMMENT '完成时间',
//     `create_time` datetime NOT NULL COMMENT '创建时间',
//     `updated_time` datetime NOT NULL COMMENT '更新时间',
//     PRIMARY KEY (`id`)
//   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

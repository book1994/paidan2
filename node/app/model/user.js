const dayjs = require("dayjs");
module.exports = app => {
    const { INTEGER, STRING, DECIMAL } = app.Sequelize;
    const User = app.model.define(
        'user',
        {
            id: {type: INTEGER, primaryKey: true, autoIncrement: true},
            username: STRING, // 账号
            nickname: STRING, // 昵称
            password: STRING, // 密码
            avatar_url: STRING, // 头像
            role: {
                type: INTEGER,
                defaultValue: 0, // 角色0普通用户、1员工、2管理员
            },
            phone: STRING, // 手机
            description: STRING, // 描述
            balance: DECIMAL(10, 2), // 余额
            is_audit: {
                type: INTEGER,
                defaultValue: 0, // 是否审核人员
            },
            status: {
                type: INTEGER,
                defaultValue: 1, // 状态
            },
            create_time: {
                type: app.Sequelize.DATE,
                allowNull: false,
                get() {
                    return dayjs(this.getDataValue('create_time')).format('YYYY-MM-DD');
                },
            },
            updated_time: {
                type: app.Sequelize.DATE,
                allowNull: false,
                get() {
                    return dayjs(this.getDataValue('updated_time')).format('YYYY-MM-DD');
                },
            }
        },
        {
            tableName: 'user',
            timestamps: true, // 启用时间戳(createdAt, updatedAt)，但我们将通过下面的选项重命名它们
            createdAt: 'create_time', // Sequelize将自动填充的createdAt字段重命名为create_time
            updatedAt: 'updated_time', // Sequelize将自动填充的updatedAt字段重命名为updated_time
            // 字符集和校对规则通常在数据库连接时设置，而不是在模型定义中
            // 如果需要，可以在数据库连接配置中指定
        }
    );
    return User;
};

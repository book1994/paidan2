const dayjs = require("dayjs");
module.exports = app => {
    const { STRING, INTEGER, TEXT,DECIMAL } = app.Sequelize;
    const Task = app.model.define(
        'Task',
        {
            id: {type: INTEGER, primaryKey: true, autoIncrement: true},
            task_no: STRING(255),
            user_id: INTEGER, // 发布人
            status: INTEGER, // 任务类型 -1 待审核 0 不通过 1通过
            name: STRING(255), // 任务标题
            description: STRING(255), // 任务描述
            remark: STRING(255), // 备注
            count: INTEGER, // 任务数量
            step_list: TEXT, // 步骤列表
            price: DECIMAL(10, 2), // 价格
            amount: DECIMAL(10, 2), // 总价
            start_time: {
                type: app.Sequelize.DATE,
                allowNull: true,
                get() {
                    return dayjs(this.getDataValue('start_time')).format('YYYY-MM-DD HH:mm:ss');
                },
            },
            end_time: {
                type: app.Sequelize.DATE,
                allowNull: true,
                get() {
                    return dayjs(this.getDataValue('end_time')).format('YYYY-MM-DD HH:mm:ss');
                },
            },
            create_time: {
                type: app.Sequelize.DATE,
                allowNull: false,
                get() {
                    return dayjs(this.getDataValue('create_time')).format('YYYY-MM-DD');
                },
            },
            updated_time: {
                type: app.Sequelize.DATE,
                allowNull: false,
                get() {
                    return dayjs(this.getDataValue('updated_time')).format('YYYY-MM-DD');
                },
            }
        },
        {
            tableName: 'task',
            timestamps: true, // 启用时间戳(createdAt, updatedAt)
            updatedAt: 'updated_time', // 不想要 updatedAt
            createdAt: 'create_time', // 想要  createdAt 但是希望名称叫做 created_at
        }
    );
    Task.associate = function() {
        // 任务接单记录
        app.model.Task.hasMany(app.model.TaskRecord, { foreignKey: 'task_id', targetKey: 'id', as: 'children' });
    }
    return Task
};

const dayjs = require("dayjs");
module.exports = app => {
    const { INTEGER, STRING, DECIMAL } = app.Sequelize;
    const Balance = app.model.define(
        'balance',
        {
            id: {type: INTEGER, primaryKey: true, autoIncrement: true},
            user_id: INTEGER, // 用户id
            task_id: INTEGER, // 任务id
            task_record_id: INTEGER, // 任务记录id
            desc: STRING(255), // 描述
            type: INTEGER, // 类型
            amount: DECIMAL(10, 2), // 金额
            status: {
                type: INTEGER,
                defaultValue: 1, // 状态
            },
            create_time: {
                type: app.Sequelize.DATE,
                allowNull: false,
                get() {
                    return dayjs(this.getDataValue('create_time')).format('YYYY-MM-DD');
                },
            },
            updated_time: {
                type: app.Sequelize.DATE,
                allowNull: false,
                get() {
                    return dayjs(this.getDataValue('updated_time')).format('YYYY-MM-DD');
                },
            }
        },
        {
            tableName: 'balance',
            timestamps: true, // 启用时间戳(createdAt, updatedAt)，但我们将通过下面的选项重命名它们
            createdAt: 'create_time', // Sequelize将自动填充的createdAt字段重命名为create_time
            updatedAt: 'updated_time', // Sequelize将自动填充的updatedAt字段重命名为updated_time
            // 字符集和校对规则通常在数据库连接时设置，而不是在模型定义中
            // 如果需要，可以在数据库连接配置中指定
        }
    );
    Balance.associate = function() {
        app.model.Balance.belongsTo(app.model.User, {foreignKey: 'user_id', targetKey: 'id', as: 'user'});
    }
    return Balance;
};



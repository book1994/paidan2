/**
 * * * * * * *
 * ┬ ┬ ┬ ┬ ┬ ┬
 * │ │ │ │ │ │
 * │ │ │ │ │ └─ 周日（0 - 7）（0 或 7 是周日）
 * │ │ │ │ └─── 月份（1 - 12）
 * │ │ │ └───── 日期（1 - 31）
 * │ │ └─────── 小时（0 - 23）
 * │ └───────── 分钟（0 - 59）
 * └─────────── 秒（0 - 59，可选）
 */
exports.schedule = {
    // 每天0点执行
    cron: '0 0 0 * * *',
    // 每5s执行一次
    // interval: '1s',
    type: 'all', // 指定所有的 worker 都需要执行
};

exports.task = async function (ctx) {
    console.log('定时任务开始执行')
    ctx.service.order.cancelOrder()
};

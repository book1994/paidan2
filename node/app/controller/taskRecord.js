'use strict';
const BaseController = require('./base');
const serviceName = 'taskRecord'
// 继承baseController 并且指定serviceName
class RouterController extends BaseController {
    constructor(ctx) {
        super(ctx, serviceName);
    }

    /**
     * 接单列表
     * 完成状态：  -1 待完成、0已放弃 1 已完成
     * 审核状态： -1待审核 、0拒绝 、1通过
     */
    async acceptList() {
        const {ctx} = this;
        const {service} = ctx;
        const {query} = ctx.request;
        let where = {
        }
        const {
            status,
            user_id
        } = this.ctx.query
        if (!user_id) {
            this.ctx.success([]);
        }
        where.user_id = user_id
        // 处理状态
        if (status === 'wait') {
            // 待完成
            where.status = -1
            // audit_status 不等于-1
            where.audit_status = {
                $ne: -1
            }
        }
        if (status === 'wait_audit') {
            // 待审核
            where.status = 1
            where.audit_status = -1
        }
        if (status === 'finish') {
            // 已完成 审核通过
            where.status = 1
            where.audit_status = 1
        }
        const result = await service[this.serviceName].allList(query);
        this.ctx.success(result);
    }

    /**
     * 接单
     */
    async accept() {
        const {ctx} = this;
        const {service} = ctx;
        const {request} = ctx;
        const {body} = request;
        if (!body.user_id) {
            this.ctx.error(500,'请先登录');
        }
        if (!body.task_id) {
            this.ctx.error(500,'参数错误');
        }
        const result = await service[this.serviceName].accept(body);
        this.ctx.success(result);
    }

    /**
     * 完成订单
     */
    async finish() {
        const {ctx} = this;
        const {service} = ctx;
        const {request} = ctx;
        const {body} = request;
        if (!body.user_id) {
            this.ctx.error(500,'请先登录');
        }
        if (!body.id) {
            this.ctx.error(500,'参数错误');
        }
        let data = {
            ...body,
            status: 1,
            audit_status: -1
        }
        const result = await service[this.serviceName].update(body.id,data);
        this.ctx.success(result);
    }

    /**
     * 取消订单
     */
    async cancel() {
        const {ctx} = this;
        const {service} = ctx;
        const {request} = ctx;
        const {body} = request;
        if (!body.user_id) {
            this.ctx.error(500,'请先登录');
        }
        if (!body.id) {
            this.ctx.error(500,'参数错误');
        }
        let data = {
            ...body,
            status: 0,
        }
        const result = await service[this.serviceName].update(body.id,data);
        this.ctx.success(result);
    }

    /**
     * 待审核列表
     * 完成状态：  -1 待完成、0已放弃 1 已完成
     * 审核状态： -1待审核 、0拒绝 、1通过
     */
    async auditList() {
        const { query, service } = this.ctx;
        let { pageNum, pageSize } = query;
        let page = Number(pageNum) || 1;
        let size = Number(pageSize) || 10;
        let where = {
        }
        const {
            status,
            audit_user_id,
        } = this.ctx.query
        if (audit_user_id) {
            where.audit_user_id = audit_user_id
        }
        if (status === 'processing') {
            where.status = -1
        }
        if (status === 'wait_audit') {
            // 待审核
            where.status = 1
            where.audit_status = -1
        }
        // 已审核
        if (status === 'pass') {
            // 不等于-1
            where.audit_status = 1
        }
        // 拒绝
        if (status === 'refuse') {
            where.audit_status = 0
        }
        console.log('pageNum',where)
        const result = await service[this.serviceName].getPage(where, page, size, [
            {
                model: this.ctx.model.Task,
                as: 'task',
            },
            {
                model: this.ctx.model.User,
                as: 'user',
            },
            {
                model: this.ctx.model.User,
                as: 'auditUser',
            }
        ]);
        this.ctx.success(result);
    }

    /**
     * 审核
     * 1 通过 0 拒绝
     */
    async audit() {
        const {ctx} = this;
        const {service} = ctx;
        const {request} = ctx;
        const {body} = request;
        if (!body.user_id) {
            this.ctx.error(500,'请先登录');
        }
        if (!body.id) {
            this.ctx.error(500,'参数错误');
        }
        let data = {
            audit_status: body.audit_status,
            id: body.id,
            audit_user_id: body.audit_user_id
        }
        // 当前任务是否存在
        const taskRecord = await service[this.serviceName].findOne(body.id);
        if (!taskRecord) {
            this.ctx.error(500,'任务不存在');
        }
        console.log('taskRecord',taskRecord)
        const result = await service[this.serviceName].update(body.id,data);
        // 加钱
        await service.user.addMoney({
            user_id: taskRecord.dataValues.user_id,
            price: taskRecord.dataValues.price,
            task_id:taskRecord.dataValues.task_id,
            task_record_id:taskRecord.dataValues.id,
            desc: '任务完成奖励'
        });
        // TODO 日志
        this.ctx.success(result);
    }

    /**
     * 是否有取消过
     */
    async status() {
        const {ctx} = this;
        const {service} = ctx;
        const {query} = ctx.request;
        const {user_id,task_id} = query
        if (!user_id || !task_id) {
            this.ctx.success(500,'参数错误');
        }
        const result = await service[this.serviceName].findByValues({
            user_id,
            task_id,
        });
        this.ctx.success(result);
    }
}
module.exports = RouterController;

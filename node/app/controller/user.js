'use strict';

// const jwt = require('jsonwebtoken');
const BaseController = require('./base');
const serviceName = 'user';
class UserController extends BaseController {
    constructor(ctx) {
        super(ctx, serviceName);
    }
    // 登录
    async login() {
      const { ctx } = this;
      const { request, service } = ctx;
      const data = request.body;
      const user = await service[serviceName].login(data);
      // user.token = jwt.sign(user, this.config.keys);
      ctx.success(user);
    }
    // 指定为审核人员
    async audit() {
      const { ctx } = this;
      const { request, service } = ctx;
      const data = {
        id: request.body.id,
        is_audit: 1
      }
      const result = await service[serviceName].update(data.id, data);
      ctx.success(result);
    }
}

module.exports = UserController;

'use strict';
const BaseController = require('./base');
const serviceName = 'balance'
// 继承baseController 并且指定serviceName
class RouterController extends BaseController {
    constructor(ctx) {
        super(ctx, serviceName);
    }
    async allList() {
        const {ctx} = this;
        const {service} = ctx;
        const {query} = ctx.request;
        const result = await service[this.serviceName].allList(query);
        this.ctx.success(result);
    }
    async create2() {
        const {ctx} = this;
        const {service} = ctx;
        const {body} = ctx.request;
        const result = await service[this.serviceName].create2(body);
        if (result) {
            this.ctx.success();
        } else {
            this.ctx.fail('余额不足');
        }
    }
}
module.exports = RouterController;

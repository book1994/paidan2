'use strict';
const BaseController = require('./base');
const serviceName = 'task'
// 继承baseController 并且指定serviceName
class RouterController extends BaseController {
    constructor(ctx) {
        super(ctx, serviceName);
    }
    async index() {
        const { query, service } = this.ctx;
        let { pageNum, pageSize } = query;
        let page = Number(pageNum) || 1;
        let size = Number(pageSize) || 10;
        const where = {

        };
        if(query.status === 'doing') {
            // 状态是进行中
            where.status = -1;
        }
        if(query.status === 'done') {
            // 状态是已完成
            where.status = 1;
        }
        const list = await service[this.serviceName].getPage(where, page, size);
        this.ctx.success(list);
    }
}
module.exports = RouterController;

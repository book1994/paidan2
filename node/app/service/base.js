'use strict';
const { Service } = require('egg');
class RouterService extends Service {
    constructor(ctx, modelName) {
        super(ctx);
        this.modelName = modelName;
    }
    async getPage(where, page, pageSize,include) {
        return await this.ctx.model[this.modelName].findAndCountAll({
            where,
            offset: pageSize * (page - 1),
            limit: pageSize,
            order: [
                // id 降序
                ['id', 'DESC'],
            ],
            include
        });
    }
    async allList(where,include) {
        return this.ctx.model[this.modelName].findAll({
            where,
            order:[
                // id 降序
                ['id','DESC'],
            ],
            include
        });
    }
    async findOne(id,include) {
        return await this.ctx.model[this.modelName].findOne({
            where: {
                id,
            },
            include
        });
    }
    async create(data) {
        delete data.id
        return this.ctx.model[this.modelName].create({
            ...data,
        });
    }
    async update(id, data) {
        return await this.ctx.model[this.modelName].update(data, { where: { id } });
    }
    async deleteById(id) {
        return await this.ctx.model[this.modelName].destroy({ where: { id } });
    }
}

module.exports = RouterService;

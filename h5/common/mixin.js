export default {
    data() {
        return {
            actionStyle: {
                color: '#3996FD',
                'margin-right': '10px'
            },
            keyword: '',
            loading: 'nomore',
            pageNum: 1,
            pageSize: 10,
            totalCount: 0,
            dataList: [
            ],
            searchParams: {}
        }
    },
    computed: {
        totalPage() {
            console.log('Math.ceil(this.totalCount / this.pageSize)',Math.ceil(this.totalCount / this.pageSize))
            return Math.ceil(this.totalCount / this.pageSize)
        }
    },
    methods: {
        onScroll(e) {
            if (this.pageNum >= this.totalPage) {
                this.loading = 'nomore'
                return
            } else {
                this.loading = 'loading'
            }
            this.pageNum++
            this.getList()
        },
        getList() {},
        handleSearch() {
            this.pageNum = 1
            this.getList()
        },
        handleClearSearch() {}
    }
}

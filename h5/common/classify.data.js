export default[
   {
    "name": "美食",
    "foods": [
        {
            id: 1,
          "name": "火锅",
          "key": "火锅",
          "icon": "https://cdn.uviewui.com/uview/common/classify/2/1.jpg",
          "price": 6,
            desc: '10份起团'
        },
        {
            id: 2,
          "name": "糕点饼干",
          "key": "糕点饼干",
          "icon": "https://cdn.uviewui.com/uview/common/classify/2/2.jpg",
          "price": 6,
            desc: '10份起团'
        },
        {
            id: 3,
          "name": "坚果果干",
          "key": "坚果果干",
          "icon": "https://cdn.uviewui.com/uview/common/classify/2/3.jpg",
          "price": 6,
            desc: '10份起团'
        },
        ]
    },
]
// export const baseUrl = ''
export const baseUrl = 'http://62.234.165.96:7001'
import store from '../store'
const config = {
    baseUrl: baseUrl + '/api',
    loadingText: '加载中~',
    loadingTime: 2000,
    header: {
        Accept: 'application/json',
    }
}

const install = (Vue, vm) => {
    // 此为自定义配置参数，具体参数见上方说明
    Vue.prototype.$u.http.setConfig({
      ...config
    });
    // 请求拦截部分，如配置，每次请求前都会执行
    Vue.prototype.$u.http.interceptor.request = (config) => {
        // if(!config.url.includes('login') && !uni.getStorageSync('token')) {
        //     vm.$u.toast('登录过期, 请重新登录');
        //     setTimeout(() => {
        //         uni.reLaunch({
        //             url: '/pages/user/login'
        //         })
        //     }, 1000)
        //     return false;
        // }
        uni.showLoading({
            title: config.loadingText
        })
        config.header['token'] = uni.getStorageSync('token')
        return config;
    }

    // 响应拦截，如配置，每次请求结束都会执行本方法
    Vue.prototype.$u.http.interceptor.response = (res) => {
        uni.hideLoading()
        if (res.msg === '用户不存在') {
            return false
        }
        if(res.code === 200) {
            // res为服务端返回值，可能有code，result等字段
            // 这里对res.result进行返回，将会在this.$u.post(url).then(res => {})的then回调中的res的到
            // 如果配置了originalData为true，请留意这里的返回值
            return res.data;
        } else if(res.code === 400) {
            // 假设201为token失效，这里跳转登录
            vm.$u.toast('登录过期, 请重新登录');
            setTimeout(() => {
                uni.reLaunch({
                    url: '/pages/user/login'
                })
            }, 1000)
            return false;
        } else {
            vm.$u.toast(res.msg);
            // 如果返回false，则会调用Promise的reject回调，
            // 并将进入this.$u.post(url).then().catch(res=>{})的catch回调中，res为服务端的返回值
            return false;
        }
    }
}

export default {
    install
}

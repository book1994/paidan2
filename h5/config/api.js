const http = uni.$u.http
export default {
    // 员工
    login: (data) => http.post('/user/login', data),
    userDetail: (id) => http.get(`/user/${id}`),
    userUpdate: (data) => http.put(`/user/${data.id}`, data),
    userAdd: (data) => http.post('/user', data),
    userList: (params) => http.get('/user/list', params),
    delUser: (id) => http.delete(`/user/${id}`),
    /**
     * 我的任务
     */
    getTaskPage: (params) => http.get('/task', params),
    createTask: (data) => http.post('/task', data),
    getTaskDetail: (id) => http.get(`/task/${id}`),
    getTaskRecordStatus: (params) => http.get(`/taskRecord/status`,params),
    getTaskRecord: (params) => http.get('/taskRecord/' + params.id, params),
    acceptTask: (data) => http.post(`/taskRecord/accept`, data),
    cancelTask: (data) => http.post(`/taskRecord/cancel`, data),
    finishTask: (data) => http.post(`/taskRecord/finish`, data),
    // 任务列表
    getTaskRecordAuditPage: (params) => http.get('/taskRecord/auditList', params),
    auditTaskRecord: (data) => http.post(`/taskRecord/audit`, data),
    balanceList: (params) => http.get('/balance/list', params),
    createBalance: (data) => http.post('/balance/create2', data),
    updateBalance: (data) => http.put(`/balance/${data.id}`, data),
    deleteTask: (id) => http.delete(`/task/${id}`),

}

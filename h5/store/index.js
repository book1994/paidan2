import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		token: uni.getStorageSync('token') || '',
		userInfo: uni.getStorageSync('userInfo') || {},
		role: uni.getStorageSync('role') || '',
	},
	getters: {
		userInfo: state => state.userInfo,
		token: state => state.token,
		role: state => state.role,
	},
	mutations: {
		SET_USERINFO(state, userInfo) {
			state.userInfo = userInfo
			state.token = userInfo.token
			uni.setStorageSync('userInfo', userInfo || {})
			uni.setStorageSync('token', userInfo.token || '')
		},
		SET_ROLE(state, role) {
			state.role = role
			uni.setStorageSync('role', role || 0)
		},
		logout(state) {
			state.userInfo = {}
			state.token = ''
			state.role = ''
			uni.removeStorageSync('userInfo')
			uni.removeStorageSync('token')
			uni.removeStorageSync('role')
		}
	},
	actions: {

	}
})

export default store

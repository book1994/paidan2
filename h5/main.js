import App from './App'
import store from './store'
// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
Vue.config.productionTip = false
App.mpType = 'app'
import uView from "uview-ui";
Vue.use(uView);
import $api from 'config/api'
Vue.prototype.$api = $api
const app = new Vue({
  ...App
})
import httpInterceptor, { baseUrl } from '@/common/http.interceptor.js'
Vue.prototype.$baseUrl = baseUrl
Vue.prototype.$store = store;
Vue.prototype.$errMsg = function (err) {
  const regex = /<p>(.*?)<p>/g;
  const result = err.data.match(regex);
  if (result) {
    app.$u.toast(result[0].replace(/<[^>]+>/g, ''));
  } else {
    app.$u.toast('服务端报错了...');
  }
}
Vue.prototype.getFileIcon = function (fileName) {
  let ext = fileName.split('.').pop()
  if (ext === 'pdf') {
    return '/static/icon/pdf.png'
  } else if (ext === 'doc' || ext === 'docx') {
    return '/static/icon/doc.png'
  } else if (ext === 'xls' || ext === 'xlsx') {
    return '/static/icon/xls.png'
  } else if (ext === 'ppt' || ext === 'pptx') {
    return '/static/icon/ppt.png'
  } else if (ext === 'jpg' || ext === 'jpeg' || ext === 'png' || ext === 'gif') {
    return fileName
  }
}
const prePage = ()=>{
  let pages = getCurrentPages();
  let prePage = pages[pages.length - 2];
  // #ifdef H5
  return prePage;
  // #endif
  return prePage.$vm;
}
Vue.prototype.$prePage = prePage
Vue.use(httpInterceptor, app)
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif
